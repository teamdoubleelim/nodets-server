import axios from 'axios';
import querystring from 'querystring';

// cSpell:ignore subdomain, lokijs, challonge

export class EventService {

  // Info for the Challonge API
  users: string[];
  apiKey: string;
  subdomain: string;
  name: string;
  id: string;
  url: string;
  usersId: string;
  currentStep: string = 'create';
  // Date info to append to the url to keep it random
  today: Date = new Date();
  dd: string = BracketUtility.getValidDay(this.today);
  mm: string = BracketUtility.getValidMonth(this.today);
  yyyy: number = this.today.getFullYear();

  validDate: string = BracketUtility.getValidDate(this.dd, this.mm, this.yyyy);
  randomNumber: number = BracketUtility.getRandomNumber();

  // Challonge API URLs
  createBracketUrl: string = `https://api.challonge.com/v1/tournaments.json?`;
  addUsersUrl: string = `https://api.challonge.com/v1/tournaments/`;

  createBracket(bracketName: string, bracketFormat: string): void {

    // POSTs to the Challonge API to create the bracket with the info from the UI
    axios.post(this.createBracketUrl, querystring.stringify({
      'api_key': this.apiKey,
      'tournament[subdomain]': this.subdomain,
      'tournament[url]': `${this.subdomain}${this.validDate}${this.randomNumber}`,
      'tournament[name]': bracketName
    }))
    .then(res => {
      this.id = res.data.tournament.id;
      this.url = res.data.tournament.url;
      console.log(`Bracket has been created with name ${this.name} under the subdomain ${this.subdomain}`);
    })
    .catch(err => err);
  }

  addUsers(users: [{name: string, phone: string}]): any {

    // POSTs to the Challonge API all of the users to add to the event
    const nameList: Array<string> = [];
    users.map(user => nameList.push(user.name));
    this.addUsersUrl += `${this.id}/participants/bulk_add.json`;

    axios.post(this.addUsersUrl, querystring.stringify({
      'api_key': this.apiKey,
      'participants[][name]': nameList
    }))
    .then(res => {
      console.log(`Player(s) ${nameList} have successfully been added`);
    })
    .catch(err => err);
  }
}

class BracketUtility {
  static getRandomNumber(): any {
    return Math.floor(Math.random() * (999 - 0));
  }

  static prePendZeroIfSingleDigitDate(date: number): any {
    return date < 10
    ? `0${date}`
    : `${date}`;
  }

  static getValidDay(today: Date): any {
    return this.prePendZeroIfSingleDigitDate(today.getDate());
  }

  static getValidMonth(today: Date): any {
    const month = today.getMonth() + 1;
    return this.prePendZeroIfSingleDigitDate(month);
  }

  static getValidDate(day: string, month: string, year: number): any {
    return `${day}${month}${year}`;
  }
}

export default Bracket;